module.exports = function(grunt) {

	grunt.initConfig ({
		pkg: grunt.file.readJSON('package.json'),
		//use uglify to minify JavaScript files
		uglify: {
			options: {
				mangle:false,
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
			},
			my_target: {
				files: {  //specify the output file and src file here as   'PathTo/output.min.js' : ['PathTo/input.js']
					'public/js/group07.min.js': ['public/js/group07.js'],
					'public/js/jquery.easing.1.3.js': ['public/js/jquery.easing.1.3.js']
				}
			}	
		},

		//use htmlmin to minify html files
		htmlmin : {
			dist: {
				options: {
					removeComments: true,
					collapseWhitespace:true
				},
				files: {     // 'destination': 'source'      note: you can have multiple files here
					'views/index.min.ejs' : 'views/index.ejs',
					'views/reminder.min.ejs' : 'views/reminder.ejs'
				}
			}
		},

		//use cssmin to minify and combine css files
		cssmin: {
			minify: {
				expand: true,
				cwd: 'public/stylesheets/',
				src: ['*.css', '!*.min.css'],
				dest: 'public/stylesheets',
				ext: '.min.css'
			}
		},

		//need to point to a folder contains all the test files, what's shown below is just an example
		qunit: {
			all: ['test/*.js']
		},

		//just an example
		shell: {
			'git-push-heroku' : {
				command: 'git push heroku master '
			},
			'git-commit' : {
				command: 'git commit -am"build"'
			}
		}
	});

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-uglify');  //js
	grunt.loadNpmTasks('grunt-contrib-htmlmin'); //html
	grunt.loadNpmTasks('grunt-contrib-cssmin');  //css
	grunt.loadNpmTasks('grunt-contrib-qunit');   //Qunit testing
	grunt.loadNpmTasks('grunt-shell');

	// Default task(s). minify only
  	grunt.registerTask('default', ['uglify', 'htmlmin', 'cssmin']);

  	//with qunit and shell commands
  	//grunt.registerTask('default', ['uglify', 'htmlmin', 'cssmin', 'qunit', 'shell:git-push-heroku', 'shell:git-commit']);

}